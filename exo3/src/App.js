
import './App.css';
//import React, { useState } from "react";
function App(){
  const fruits = ["apple", "banana", "grapes", "mango", "orange","clémentine"];

  return(
    <div className="App">
    {fruits.map((item, index) => (
          <div key={index}>{item}</div> //Display each item
    ))}
    </div>
  ////
  );
}

export default App;
