import { Component } from "react";
import { ThemeContext } from "./App";
export default class ClassContext extends Component{
    themeStyle(dark){
        return{
        backgroundColor:dark?'#000' : '#FFF',
        color:dark?'#FFF' : '#000'
        
        }
    }
    render(){
        return(
            <ThemeContext.Consumer>
                {darkTheme=>{
                    return <div style={this.themeStyle(darkTheme)}>class </div>
                }

                }

            </ThemeContext.Consumer> 
        )
    }
}