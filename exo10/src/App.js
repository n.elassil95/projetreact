//import logo from './logo.svg';
import './App.css';
import ClassContext from './ClassContext';
import React, { useState } from "react";
//import { useEffect } from 'react';
export const ThemeContext = React.createContext()
export default function App(){
  const [darkTheme, setDarkTheme]= useState (true)
  function toggleTheme(){
    setDarkTheme(prevDarkTheme => !prevDarkTheme)
  }
  return(
    <ThemeContext.Provider value={darkTheme}>
      <button onClick={toggleTheme}>toggleTheme </button>
      <ClassContext/> 
    </ThemeContext.Provider> 
  )
}
//
/*function App() {

  const [latitude ,setlatitude] =useState ("");
  const [longitude ,setlongitude] =useState ("");
  const [data ,setData] =useState ([]);
  useEffect(()=>{
    fetch(`https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&hourly=temperature_2m`)
    .then(response => response.json())
    .then( data => setData(data) );
  },[latitude,longitude]);

  const handleChangelatitude = (event) => {
    setlatitude({
     latitude:event.target.value
    });
    console.log(latitude);
  };
 const handleChangelongitude = (event) => {
    setlongitude({
      longitude:event.target.value
    });
    console.log(longitude);
  };
  return (
    <div className="App">
             <div class="form-group row">
            <label class="col-form-label col-sm-2" for="latitude">latitude</label>
            <div class="col-sm-7">
                <input class="form-control" id="latitude" onChange={handleChangelatitude} />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label col-sm-2" for="longitude">longitude</label>
            <div class="col-sm-7">
                <input class="form-control" id="longitude" onChange={handleChangelongitude} />
            </div>
        </div>
        <br/> 
       
   

    latitude: ${data.latitude}  <br/>
    longitude:${data.longitude}<br/>
    hourly_units:${data.hourly_units.temperature_2m}<br/>
    hourly-temperature_2m:${data.hourly.temperature_2m[0]}
    
      
     
      
    </div>
  );
}

export default App;*/
