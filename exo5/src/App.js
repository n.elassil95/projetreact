//import logo from './image';
import './App.css';
import React, { useState } from "react";

function App() {

const [index, setIndex]= useState (1);
const handleClickIndexPlus=()=>{
  setIndex(index+1);
}
const handleClickIndexMois=()=>{
  setIndex(index-1);
}

  return (
    <div className="App">
        <button onClick={handleClickIndexPlus}> +</button>
          <img src={`./image/${index}.png`} className="image" alt="image" />
        <button onClick={handleClickIndexMois}> -</button>
    </div>
  );
}
////
export default App;
