//import logo from './logo.svg';
import './App.css';
import { Component } from 'react';
///
/*function App() {
  const tab=[];
  function ajouterElement(arr,element) {
    return arr.push(element);
  }
  const [langue,setlangue] = useState(tab);
  const handleChange = (event) => { 
   // console.log(event.target.value);
   if (event.target.value === "") {
    setlangue(tab);
    return;
  }
  const temp = ajouterElement(tab,event.target.name)
  setlangue(temp);
  console.log(tab);
    } 
   

  
  /*
<h2>vous parlez {langue &&langue.map((item, index) => (
          <div key={index}> {item}</div> //Display each item
          ))}</h2> */
/*return (
  <div className="App">
 <h2>vous parlez {langue}</h2> 
 <div>
    <input type="checkbox" id="Français" name="Français" onChange={handleChange} />
    <label>Français</label>
  </div>

  <div>
    <input type="checkbox" id="Anglais" name="Anglais" onChange={handleChange} />
    <label>Anglais</label>
  </div>
   <div>
    <input type="checkbox" id="Arabre" name="Arabre" onChange={handleChange} />
    <label>Arabre</label>
  </div>
  </div>
);
}
*/

class App extends Component {
  state = {
    Français: false,
    Anglais: false,
    Arabre: false
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.checked });
  }
  render() {
    const { Français ,Anglais,Arabre } = this.state;
  
    return (
      <div className="App">

        <div>
          <input type="checkbox" id="Français" name="Français" checked={Français} onChange={this.handleChange} />
          <label>Français</label>
        </div>

        <div>
          <input type="checkbox" id="Anglais" name="Anglais" checked={Anglais} onChange={this.handleChange} />
          <label>Anglais</label>
        </div>
        <div>
          <input type="checkbox" id="Arabre" name="Arabre" checked={Arabre} onChange={this.handleChange} />
          <label>Arabre</label>
        </div>
        <h2>vous parlez Français: {Français ? "oui" : "non"}</h2>
        <h2>vous parlez Anglais: {Anglais ? "oui" : "non"}</h2>
        <h2>vous parlez Arabre: {Arabre ? "oui" : "non"}</h2>
      </div>
    );
  }
}
export default App;
