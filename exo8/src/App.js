//import logo from './logo.svg';
import './App.css';
import React, { useState } from "react";
import { useEffect } from 'react';
function App() {
  const [data ,setData] =useState ([]);
  useEffect(()=>{
    fetch('https://randomfox.ca/floof/')
    .then(response => response.json())
    .then( data => setData(data) );
  },[]);
 
  return (
    <div className="App">
     
      <img src={data.image} alt="blabla"/>
     
      
    </div>
  );
}

export default App;
