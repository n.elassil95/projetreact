//import logo from './logo.svg';
import './App.css';
import React, { useState } from "react";

function filterItems(arr, query) {
  return arr.filter((el) => el.toLowerCase().includes(query.toLowerCase()));
}

function App(){
  const fruits = ["apple", "banana", "grapes", "mango", "orange","clémentine"];
const [fruitsListe, setFruitsListe]= useState (fruits);
  //
const handleClick = (event) => {

  if (event.target.value === "") {
    setFruitsListe(fruits);
    return;
  }
  const filteredValues = filterItems(fruits, event.target.value)
  setFruitsListe(filteredValues);
};
  return(
    <div className="App">
    <input type="text" placeholder='recherche...' id="recherche" name="recherche" onChange={handleClick} /> 
    {fruitsListe &&
        fruitsListe.map((item, index) => (
          <div key={index}>{item}</div> //Display each item
        ))}
    </div>
  
  );
}


export default App;
