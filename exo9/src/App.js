import './App.css';
import React from 'react';
import { Routes, Route } from 'react-router-dom';
 
import Home from './components/Home';

import About from './components/About';
import Navbar from './components/Navbar/Navigation';
//import Contact from './components/Contact';
//import Error from './components/Error';

function App(){

  return ( 
    <div className='App'>
       <Navbar/> 
 <Routes>  
 <Route path="/" element={<Home />}/>
 <Route path="/About" element={<About />}/>
 </Routes>


    </div>     
 );
}
 

export default App;

