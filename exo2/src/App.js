//import logo from './logo.svg';
import { useState } from 'react';
import './App.css';

function App(){
  const [counter, setCounter]= useState(0);
  const handleClickPlus=()=>{
    setCounter(counter+1);
  }
  const handleClickMois=()=>{
    setCounter(counter-1);
  }
  return(
    <div className="App">

<button onClick={handleClickPlus}> +</button>
<button onClick={handleClickMois}> -</button>
<h2>Counter:{counter}</h2>
    </div>
  );
}
export default App;
///