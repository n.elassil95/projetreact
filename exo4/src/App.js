//import logo from './logo.svg';
import React from 'react';
import './App.css';
import { useState } from 'react';

//import {Navigation} from 'react-minimal-side-navigation';
//import 'react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css';

function App() {
  const [selecter , setselecter]= useState(null)
  const toggle= (i)=>{
    if(selecter ===i){
 return setselecter(null)
    }
    setselecter(i)
  }
    return (
      <div className="wrapper" > some content <br/>
       <div className='accordion'>{data.map((item,i)=>(
        <div className='item'>
          <div className='title' onClick={()=> toggle(i)}>
          <h2>{item.article}</h2>
          <span>{selecter === i? "-":"+"}</span>
          </div>
          <div className={selecter === i? "content.show":"content"}>{item.content}</div>
        </div>
       ))}</div>
      </div>
    );
}
const data =[
  {
    article : 'React',
    content:`React (aussi appelé React.js ou ReactJS) est une bibliothèque JavaScript libre
     développée par Facebook (maintenant Meta) depuis 2013. Le but principal de cette bibliothèque
      est de faciliter la création d'application web monopage, via la création de composants dépendant
       d'un état et générant une page (ou portion) HTML à chaque changement d'état.
    React est une bibliothèque qui ne gère que l'interface de l'application,
     considéré comme la vue dans le modèle MVC. Elle peut ainsi être utilisée avec une autre 
     bibliothèque ou un framework MVC comme AngularJS. La bibliothèque se démarque de ses concurrents 
     par sa flexibilité et ses performances, en travaillant avec un DOM virtuel et en ne mettant à
      jour le rendu dans le navigateur qu'en cas de nécessité2. `
  },
  { article : 'Nodejs',
    content:`NodeJS est un outil libre codé en Javascript et orientée pour des applications en réseau.
     Si vous êtes sur cette page, c’est certainement parce que vous voulez avoir des explications plus
      détaillées sur NodeJS. Cet outil JavaScript est devenu célèbre dans l’univers du développement
       web depuis quelques années. D’ailleurs, il est très apprécié des géants du web comme Netflix,
        PayPal, LinkedIn, Uber, la NASA, etc. Cet article basé sur la définition de NodeJS se donne 
        pour rôle de vous faire découvrir de long en large cette technologie. Vous y trouverez 
        également tous les avantages liés à son utilisation.`

  }
]
export default App;
